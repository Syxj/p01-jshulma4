//
//  ViewController.m
//  Hello World
//
//  Created by Jordan Shulman on 1/20/17.
//  Copyright © 2017 Jordan Shulman. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize message;
@synthesize counterLabel;
@synthesize achMess;
@synthesize achUnlock;

int counter = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonClick:(id)sender
{
    [achUnlock setHidden: YES];
    [achMess setHidden: YES];
    NSLog(@"Button clicked");
    counter++;
    NSLog(@"Counter at: %d", counter);
    [counterLabel setText:[NSString stringWithFormat:@"Times Clicked: %d", counter]];
    switch(counter)
    {
        case 1:
            [message setText:@"I said not to do that!"];
            [counterLabel setHidden: NO];
            [achUnlock setHidden: NO];
            [achMess setText:@"You successfully ignored the button"];
            [achMess setHidden: NO];
            break;
        
        case 5:
            [message setText:@"Are you just ignoring me?"];
            [achUnlock setHidden: NO];
            [achMess setText:@"Starting to have fun"];
            [achMess setHidden: NO];
            break;
            
        case 15:
            [message setText:@"Was it the achievements?"];
            [achUnlock setHidden: NO];
            [achMess setText:@"Yeah"];
            [achMess setHidden: NO];
            break;
            
        case 30:
            [message setText:@"I give up. Have fun."];
            [achUnlock setHidden: NO];
            [achMess setText:@"Achievement Hunter"];
            [achMess setHidden: NO];
            break;
            
        case 50:
            [achUnlock setHidden: NO];
            [achMess setText:@"The final achievement!"];
            [achMess setHidden: NO];
            break;

    }
}

@end
