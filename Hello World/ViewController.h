//
//  ViewController.h
//  Hello World
//
//  Created by Jordan Shulman on 1/20/17.
//  Copyright © 2017 Jordan Shulman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutlet UILabel *message;
@property (nonatomic, strong) IBOutlet UILabel *counterLabel;
@property (nonatomic, strong) IBOutlet UILabel *achUnlock;
@property (nonatomic, strong) IBOutlet UILabel *achMess;



-(IBAction)buttonClick:(id)sender;

@end

