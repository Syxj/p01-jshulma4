//
//  main.m
//  Hello World
//
//  Created by Jordan Shulman on 1/20/17.
//  Copyright © 2017 Jordan Shulman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
